﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageChange
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Title = "图片轮播";
            Thread thread = new Thread(Start); ;
            thread.Start();
        }
    //图片序号
    public int i { get; set; } = 1;
    //随机对象
    Random random = new Random();
    //启动状态
    public bool flag { get; set; } = false;
    /// <summary>
    /// 委托实现修改图片的地址
    /// </summary>
    delegate void ChangeImage();
    /// <summary>
    /// 线程
    /// </summary>
    public void Start()
    {
        while (true)
        {
            if (flag)
            {

                int num = random.Next(1, 6);
                if (i == num)
                {
                    if (num == 5)
                    {
                        num = 1;
                    }
                    else
                    {
                        num += 1;
                    }
                }
                i = num;
                ChangeImage change = new ChangeImage(SetImage);
                //异步给Image1赋值
                Image1.Dispatcher.BeginInvoke(change);
                Delay(1000);
            }
        }
    }
    /// <summary>
    /// Image的动画效果
    /// </summary>
    public void SetImage()
    {
        DoubleAnimation daV2 = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromSeconds(1)));
        DoubleAnimation daV = new DoubleAnimation(1, 1, new Duration(TimeSpan.FromSeconds(1)));
        Image1.BeginAnimation(UIElement.OpacityProperty, daV2);
        Delay(500);

        Image1.Source = new BitmapImage(new Uri("/Images/" + i + ".png", UriKind.Relative));
        Image1.BeginAnimation(UIElement.OpacityProperty, daV);
        Delay(500);
    }
    /// <summary>
    /// 设置延迟方法
    /// </summary>
    /// <param name="mm"></param>
    public void Delay(int mm)
    {
        DateTime current = DateTime.Now;
        while (current.AddMilliseconds(mm) > DateTime.Now)
        {
            DispatcherHelper.DoEvents();
        }
        return;
    }
    private void Button_Click(object sender, RoutedEventArgs e)
        {
            flag = true;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            flag = false;
        }
    }
}
